﻿using ExcelDataReader;
using ProfMarkupPlatform.Module.BusinessObjects;
using ProfMarkupPlatform.Module.ComponentAbstract;
using ProfMarkupPlatform.Module.ComponentAuxiliarity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProfMarkupPlatform.Module.ComponentImplementation
{
     public class ExcellReaderComponent: IExcelReaderComponent
    {
        private BlockingCollection<DataSet> files_dset;

        public ExcellReaderComponent()
        {
            files_dset = new BlockingCollection<DataSet>();
        }

        public List<T> ReadExcelProduct<T>(string[] filePaths) where T:new()
        {
            List<T> lst_prod = new List<T>();
           
            var stage1 = Task.Run(() =>
            {
                try
                {
                    foreach (var filePath in filePaths)
                    {
                        using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
                        {
                            using (var reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                files_dset.Add(reader.AsDataSet());

                            }
                        }
                    }
                }
                finally
                {
                    files_dset.CompleteAdding();
                }

            });

            var stage2 = Task.Run(() =>
            {

                foreach( var dset in files_dset.GetConsumingEnumerable())
                {
                    foreach (DataTable dt in dset.Tables)
                    {
                        try
                        {
                            foreach (DataRow row in dt.Rows)
                            {
                                
                                var res = DataRowParser<T>.Prase(row);
                                lst_prod.Add(res);
                               
                            }
                        }
                        catch (NotSupportedException ex){ }
                        catch (Exception) { }

                    }
                }
               
            });

            Task.WaitAll(stage1, stage2);
            return lst_prod;
        }

    }
}
