﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.EF;
using ProfMarkupPlatform.Module.BusinessObjects;
using ProfMarkupPlatform.Module.ComponentAbstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfMarkupPlatform.Module.ComponentImplementation
{
    public class StorageComponent : IStorageComponent
    {
        private readonly EFObjectSpace _obj_space;
        public StorageComponent(IObjectSpace object_spase)
        {
            _obj_space = object_spase as EFObjectSpace;
        }

        public int SaveToDataBase<T>(IList<T> entities) where T:class,IXafEntityObject
        {
                var context = _obj_space.ObjectContext;
                var set = context.CreateObjectSet<T>();
                foreach(var entity in entities)
                {
                    set.AddObject(entity);
                }               
              return  context.SaveChanges();           
        }
    }
}
