﻿using System;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.ComponentModel;
using DevExpress.ExpressApp.EF.Updating;
using DevExpress.Persistent.BaseImpl.EF;
using DevExpress.ExpressApp.Design;
using DevExpress.ExpressApp.EF.DesignTime;
using MySql.Data.EntityFramework;

namespace ProfMarkupPlatform.Module.BusinessObjects {
	public class ProfMarkupPlatformContextInitializer : DbContextTypesInfoInitializerBase {
		protected override DbContext CreateDbContext() {
			DbContextInfo contextInfo = new DbContextInfo(typeof(ProfMarkupPlatformDbContext), new DbProviderInfo(providerInvariantName: "System.Data.SqlClient", providerManifestToken: "2008"));
            return contextInfo.CreateInstance();
		}
	}
    //[TypesInfoInitializer(typeof(ProfMarkupPlatformContextInitializer))]
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class ProfMarkupPlatformDbContext : DbContext
    {
        public ProfMarkupPlatformDbContext(String connectionString)
            : base(connectionString)
        {
        }
        public ProfMarkupPlatformDbContext(DbConnection connection)
            : base(connection, false)
        {
        }
        public ProfMarkupPlatformDbContext()
        {
            
        }
        public DbSet<ModuleInfo> ModulesInfo { get; set; }
        public DbSet<employees> employeess { get; set; }
        public DbSet<departments> departments { get; set; }
        public DbSet<products> products { get; set; }
        public DbSet<customers> customers { get; set; }
        public DbSet<orders> orders { get; set; }
        public DbSet<order_products> order_products { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<customers>()
                .HasMany(e => e.orders)
                .WithOptional(e => e.Customer)
                .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<departments>()
                 .HasMany(e => e.employees)
                 .WithOptional(e => e.department)
                 .HasForeignKey(e => e.DepartmentId);

            modelBuilder.Entity<employees>()
              .HasMany(e => e.orders)
              .WithRequired(e => e.Employee)
              .HasForeignKey(e => e.EmployeeId)
              .WillCascadeOnDelete(false);

            /*
            modelBuilder.Entity<employees>()
                .HasMany(e => e.order_products)
                .WithOptional(e => e.Employee)
                .HasForeignKey(e => e.EmployyId);*/

            modelBuilder.Entity<orders>()
             .HasMany(e => e.order_products)
             .WithRequired(e => e.Order)
             .HasForeignKey(e => e.OrderId)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<products>()
                .HasMany(e => e.order_products)
                .WithRequired(e => e.Product)
                .HasForeignKey(e => e.ProductId)
                .WillCascadeOnDelete(false);

            /*
            modelBuilder.Entity<employees>()
               .HasMany(e => e.ProductsCollection)
               .WithMany(e => e.EmployeesCollection)
               .Map(m => m.ToTable("employee_products").MapLeftKey("EmployeeId").MapRightKey("ProductId"));*/


        }

    }
}