﻿using ProfMarkupPlatform.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace ProfMarkupPlatform.Module.ComponentAbstract
{
    public interface IExcelReaderComponent
    {
        List<T> ReadExcelProduct<T>(string[] filePaths) where T : new();
    }
}
