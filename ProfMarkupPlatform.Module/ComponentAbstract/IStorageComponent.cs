﻿using DevExpress.ExpressApp;
using ProfMarkupPlatform.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfMarkupPlatform.Module.ComponentAbstract
{
    public interface IStorageComponent
    {
        int SaveToDataBase<T>(IList<T> entities) where T : class, IXafEntityObject;
    }
}
