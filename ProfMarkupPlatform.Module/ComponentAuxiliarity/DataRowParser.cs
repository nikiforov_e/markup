﻿using ProfMarkupPlatform.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfMarkupPlatform.Module.ComponentAuxiliarity
{
    class DataRowParser<T> where T:new()
    {

        public static T Prase(DataRow row)
        {
            var cells = row.ItemArray;
            if (typeof(products) == typeof(T))
            {
                var product = new products();
                   product.ProductName = cells[0].ToString();
                   product.Warranty = Convert.ToInt32(cells[1]);
                   product.Availability = cells[2].ToString();
                   product.Price = Convert.ToDecimal(cells[3]);
                   product.DeliveryTime = string.IsNullOrEmpty(cells[4].ToString())
                       ? (DateTime?)null
                       : DateTime.ParseExact(cells[4].ToString(), "d MMM HH:mm", CultureInfo.InvariantCulture);
                return (T)(object)product;
            }
            else
            {
                throw new NotSupportedException();
            }


            
           
               

            
        }

    }
}
