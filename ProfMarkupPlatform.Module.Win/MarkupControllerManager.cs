﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Core;
using DevExpress.ExpressApp.Model;
using DryIoc;
using ProfMarkupPlatform.Module.ComponentAbstract;
using ProfMarkupPlatform.Module.ComponentImplementation;
using ProfMarkupPlatform.Module.Win.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProfMarkupPlatform.Module.Win
{
    public class MarkupControllerManager : ControllersManager
    {
        protected readonly Container container = new Container();
        protected readonly Func<IObjectSpace> createObjectSpace;

      
        public MarkupControllerManager(Func<IObjectSpace> createObjectSpace)
        {
            this.createObjectSpace = createObjectSpace;
            
        }

        protected virtual void RegisterDependencies(Container container)
        {
            if (container.GetServiceRegistrations().Any())  
                return;

            container.RegisterDelegate(d => createObjectSpace());
            container.Register<IExcelReaderComponent, ExcellReaderComponent>();
            container.Register<IStorageComponent, StorageComponent>();
            
        }

        protected override Controller CreateController(Controller sourceController, IModelApplication modelApplication)
        {
            Controller controller = base.CreateController(sourceController, modelApplication);
            InterceptForDependencyInjection(controller);
            return controller;
        }

        private void InterceptForDependencyInjection(Controller controller)
        {
            IEnumerable<MethodInfo> injectMethods = controller.GetType()
                                                              .GetMethods()
                                                              .Where(m => m.Name == "Inject");
            if (!injectMethods.Any())
                return;
            RegisterDependencies(container);
            MethodInfo injectMethod = injectMethods.OrderByDescending(m => m.GetParameters().Count())
                                                   .FirstOrDefault();
            PropertyInfo objectSpaceProperty = controller.GetType()
                                                         .GetProperties(BindingFlags.NonPublic | BindingFlags.Instance)
                                                         .SingleOrDefault(p => p.Name == "ObjectSpace");
            if (objectSpaceProperty == null)
                InvokeInjection(container, injectMethod, controller);
            else
                controller.Activated += (s, e)
                                        =>
                {
                    
                    using (IResolverContext scope = container.OpenScope()) 
                    {
                        IObjectSpace objectSpaceInstance = objectSpaceProperty.GetValue(controller) as IObjectSpace;
                        scope.UseInstance(objectSpaceInstance, preventDisposal: true);
                        InvokeInjection(scope, injectMethod, controller);
                    }
                  

                };
        }
        private void InvokeInjection(IResolverContext scope, MethodInfo method, Controller controller)
        {
           
            object[] parameters = method.GetParameters()
                                        .OrderBy(p => p.Position)
                                        .Select(p => scope.Resolve(p.ParameterType))
                                        .ToArray();
            method.Invoke(controller, parameters);
        }


    }
}
