﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using ProfMarkupPlatform.Module.BusinessObjects;
using ProfMarkupPlatform.Module.ComponentAbstract;

namespace ProfMarkupPlatform.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ExcelController : ViewController
    {
        private  IExcelReaderComponent _excell_reader;
        private IStorageComponent _excel_storage;
        public ExcelController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }


        public void Inject(IExcelReaderComponent excel_reader, IStorageComponent excel_storage)  // This one is automatically executed with the desired service supplied  
        {
            _excell_reader = excel_reader;
            _excel_storage = excel_storage;
        }       

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void OpenExcelAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {           
           
            var Method = this.GetType().GetMethod("ImportData", BindingFlags.Instance | BindingFlags.NonPublic);
            var fooTMethod = Method.MakeGenericMethod(TargetObjectType);
            fooTMethod.Invoke(this, null);
        }

        private void ImportData<T>() where T: class, IXafEntityObject,new()
        {
           
             var lstView = View as DevExpress.ExpressApp.ListView;
             OpenFileDialog dialog = new OpenFileDialog();
             dialog.Multiselect = true;
             dialog.Filter = "Excel Files|*.xlsx;*.xlsm;*.xlsb;*.xls;*‌​.xml;";
             dialog.Title = "Импорт товаров из Excel файла";
             if (dialog.ShowDialog() == DialogResult.OK)
             {
                var lst_product=_excell_reader.ReadExcelProduct<T>(dialog.FileNames);
            
                if (_excel_storage.SaveToDataBase(lst_product) > 0)
                {
                    lstView.ObjectSpace.Refresh();
                }
             }

            

        }




    }
}
